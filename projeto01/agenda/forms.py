from django import forms
from .models import Cadastro

class CadastroForm(forms.ModelForm):
    class Meta:
        # Pega os dados do MODELO
        model = Cadastro
        # Usa todos os campos
        fields = '__all__'
        # Faz com que não seja usado o campo 'user' Usuário
        exclude = ['user']
        widgets = {'nome': forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'Nome'}),
                   'email': forms.EmailInput(attrs={'class': 'form-control', 'type': 'email', 'placeholder': 'E-mail'}),
                   'endereco': forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'Endereço'}),
                   'cargo': forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'Cargo'}),
                   'celular': forms.TextInput(attrs={'class': 'form-control', 'type': 'tel', 'placeholder': 'Celular'}),
                   'telefone': forms.NumberInput(attrs={'class': 'form-control', 'type': 'number', 'placeholder': 'Telefone'}),
                   'sexo': forms.Select(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'Sexo'})}

