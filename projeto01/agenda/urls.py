from django.urls import path
from . import views

# adicionar um 'app_name'
app_name = "agenda"


urlpatterns = [
    path('', views.lista_contato, name='lista'),
    path('cadastrar/', views.novo_contato, name='cadastrar'),
    path('editar_cadastro/<int:pk>/', views.editar_contato, name='editar_contato'),
    path('excluir_contato/<int:pk>/', views.deletar_contato, name='deletar_contato'),
    
]