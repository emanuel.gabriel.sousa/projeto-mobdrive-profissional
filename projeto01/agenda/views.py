from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages

# Importando o Forms e os Modelos
from .forms import CadastroForm
from .models import Cadastro


# Criar uma função para listar todos os contatos
@login_required
def lista_contato(request):
    name_template = "agenda/cadastro_list.html"
    cadastro = Cadastro.objects.filter(user=request.user)
    return render(request, name_template, {'cadastro': cadastro})


@login_required
def editar_contato(request, pk):
    nome_template = "agenda/cadastro_form.html"
    contato = get_object_or_404(Cadastro, id=pk, user=request.user)
    if request.method == 'POST':
        form = CadastroForm(request.POST, instance=contato)
        if form.is_valid():
            form.save()
            return redirect('agenda:lista')
    else:
        form = CadastroForm(instance=contato)
    return render(request, nome_template, {'form': form})


# Criar a função que irá deletar/excluir um contato
@login_required
def deletar_contato(request, pk):
    nome_redirect = 'agenda:lista'
    nome_template = "agenda/cadastro_list.html"
    contato = Cadastro.objects.get(id=pk)
    if contato.user == request.user:
        contato.delete()
    else:
        messages.error(request, 'Você não tem permissão para excluir este contato')
        return render(request, nome_template)
    return redirect(nome_redirect)



@login_required
def novo_contato(request):
    nome_template = "agenda/cadastro_form.html"
    if request.method == 'POST':
        formulario = CadastroForm(request.POST)
        if formulario.is_valid():
            f = formulario.save(commit=False)
            f.user = request.user
            f.save()
            return redirect('agenda:lista')
        else:
            print(formulario.errors)
    else:
        formulario = CadastroForm()
    return render(request, nome_template, {'form': formulario})

