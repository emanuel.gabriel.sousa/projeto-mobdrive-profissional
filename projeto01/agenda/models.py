from django.db import models
from django.contrib.auth.models import User


class Cadastro(models.Model):
    nome = models.CharField(max_length=100, verbose_name="Nome")
    email = models.EmailField(verbose_name="E-mail")
    endereco = models.CharField(max_length=255, verbose_name="Endereço")
    cargo = models.CharField(max_length=100, verbose_name="Cargo")
    celular = models.CharField(max_length=9, verbose_name="Celular")
    telefone = models.CharField(max_length=9, verbose_name="Tel.Fixo")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    SEXO = (
        ('Masculino', 'Masculino'),
        ('Feminino', 'Feminino'),
    )
    sexo = models.CharField(max_length=10, choices=SEXO)

    def __str__(self):
        return self.nome
