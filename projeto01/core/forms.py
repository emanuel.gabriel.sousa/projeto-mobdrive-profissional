from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password')
        widgets = {'first_name': forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'Nome'}),
                   'last_name': forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'Sobrenome'}),
                   'email': forms.EmailInput(attrs={'class': 'form-control', 'type': 'email', 'placeholder': 'E-mail'}),
                   'username': forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'placeholder': 'Usuário'}),
                   'password': forms.PasswordInput(attrs={'class': 'form-control line-input', 'placeholder': 'Senha'})}


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = '__all__'









