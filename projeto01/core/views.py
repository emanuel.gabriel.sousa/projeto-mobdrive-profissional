from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import logout
from .forms import UserForm


# Função criada para mostrar quantos usuários está cadastrados na aplicação
def home(request):
    conta = User.objects.count()
    return render(request, 'home.html', {'conta': conta})


def home_mapa(request):
    return render(request, 'agenda/mapeamento.html')


# Função criada que irá fazer o LOGOUT na aplicação
def user_logout(request):
    logout(request)
    return redirect('login')


# Função criada que irá fazer o LOGOUT na aplicação
def msg_conta_criada(request):
    nome_template = 'registration/conta_criada_sucesso.html'
    form = UserForm()
    return render(request, nome_template, {'form': form})


# Criando a função que irá CADASTRAR/REGISTRAR usuário na aplicação
def registro(request):
    nome_template = 'registration/registro.html'
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            u = form.save()
            u.set_password(u.password)
            u.save()
            return redirect('msg_conta_criada')
    else:
        form = UserForm()
    return render(request, nome_template, {'form': form})


