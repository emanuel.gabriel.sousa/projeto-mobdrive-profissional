from django.contrib import admin
from django.urls import path, include
from .core import views

from .agenda import urls as agenda_urls

from django.conf.urls.static import static
# Para servir os CSS e JS localmente
from django.conf import settings



urlpatterns = [
    path('', views.home, name="home"),
    path('mapa/', views.home_mapa, name="home_mapa"),
    path('registro/', views.registro, name="registro"),
    path('logout/', views.user_logout, name='logout'),
    path('conta-criada/', views.msg_conta_criada, name='msg_conta_criada'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('agenda/', include(agenda_urls, namespace='agenda')),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))

]


# Configurando os arquivos estáticos a serem usandos na aplicação(CSS e JavaScripts localmente)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)