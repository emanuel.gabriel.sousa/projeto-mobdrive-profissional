/**
 * Realiazar o mapeamento do JavaScript para o HTML5
 */


// Configurando o Token para acesso ao MapBox
const token = 'pk.eyJ1IjoiZW1hbnVlbC1nYWJyaWVsIiwiYSI6ImNqcGsycngzZDBkNnQzcHJrZ3E0N3d1ZG0ifQ.Pki1OYx4rMnJFQicSAxUhw';

//Declarando as variáveis
const distancia = 10;
var local = "";
var destino = "";
var qtdMotoristas = 110;
var qtdPassageiros = 150;

mapboxgl.accessToken = token;

var map = new mapboxgl.Map({
    container: 'map', // Container ID
    style: 'mapbox://styles/mapbox/streets-v10', // Map style to use 
    center: [-42.802671, -5.078346], // Starting position [lng, lat] ---> -5.078346, -42.802671
    zoom: 13, // Inicializando o nível zoom
    bearing: 4,
});


// Habilitando o Marker para exibir no MAPA
var marker = new mapboxgl.Marker() // Inicializar um novo marker
    .setLngLat([-42.802671, -5.078346]) // Marcar a latitude e longitude coordenadas  
    .addTo(map); // Adicionar o marcador para ao MAPA
    
map.addControl(new MapboxGeocoder({
    // Inicializar o geocoder
    accessToken: mapboxgl.accessToken, // Setar o acesso ao token
    placeholder: "Pesquisar seu destino", // Texto de espaço reservado para a barra de pesquisa
    proximity: {
      longitude: -42.802671,
      latitude: -5.078346,
      trackProximity: true
    }
  })
);

var geocoder = new MapboxGeocoder({
  // Inicializar o geocoder
  accessToken: mapboxgl.accessToken, // Setar o acesso ao token
  placeholder: "Pesquisar seu destino", // Texto de espaço reservado para a barra de pesquisa
    bbox: [-42.802671, -5.078346, -42.802671, -5.078346],
  proximity: {
    longitude: -42.802671,
    latitude: -5.078346,
    trackProximity: true
  }
});

map.addControl(geocoder);


// Adicionar a localização do aparelho
map.addControl(new mapboxgl.GeolocateControl({
    positionOptions: {
        enableHighAccuracy: true
    },
    trackUserLocation: true
}));


// Habilitando o botão de Scroll
map.addControl(new mapboxgl.ScaleControl());
// Habilitar o botão de Navegação
map.addControl(new mapboxgl.NavigationControl());
//Habilitar o botão FullScreen para maximizar a tela - VIEW
map.addControl(new mapboxgl.FullscreenControl());



/**
 * Será elevado o preço nos horários de PICO(ALTA DEMANDA),
 * onde existe uma demanda muito grande e uma oferta um pouco mais baixa, 
 * faturando muito mais pelos mesmos serviços.
 */
function quantidadeMotorista() {
    if (qtdMotoristas < qtdPassageiros) {
        alert('Preço deve subir pois a oferta(viagem/solicitação) é maior do que a demanda');
    } else if(qtdMotoristas => qtdPassageiros){
        alert('Demanda deverá voltar ao normal')
    }
}


